<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
    	<div class="navbar-header">
    		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
    		</button>
    		<a class="navbar-brand" href="{{route('product.index')}}">Brand</a>
    	</div>

    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    		<ul class="nav navbar-nav">
				<li class="active pull-right"><a href="{{route('product.shoppingcart')}}">Cart<span class="badge badge-default">				
				@if(Session::has('cart'))
					{{Session::get('cart')->quantity}}
									@endif
									</span></a>
				

</li>
				@if(Auth::check())
				<li class="active pull-right"><a href="{{ route('user.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
<form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
@else
				<li class="active pull-right"><a href="{{route('user.signup')}}">Signup</a></li>
				<li class="active pull-right"><a href="{{route('user.signin')}}">Signin</a></li>
				@endif


    		</ul>
    	</div>
	</div>
</nav>