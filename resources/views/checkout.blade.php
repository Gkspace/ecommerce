@extends('layouts.master')

@section('content')

<div class="row">

  <div class="col-sm-6">
  <h4>Delivery Details</h4>
  <form action="{{route('product.checkout')}}" method="post">
    <div class="form-group">
                      <label class="form-control-label" for="imagepath">Name</label>

         <input type="text" id="username" placeholder="Name" class="form-control" name="username">
         </div>
     <div class="form-group">
                           <label class="form-control-label" for="imagepath">Address</label>
       <input type="text" id="address" placeholder="Address" class="form-control" name="address">
     </div>
     {{ csrf_field() }}
        <input type="submit" class="btn btn-info" value="Place Order">

  </form>

</div>

@endsection