@extends('layouts.master')

@section('content')
@foreach ($products->chunk(3) as $productChunk)

<div class="row">
@foreach ($productChunk as $product)

  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="{{$product->imagepath}}" alt="..." height="350px" width="350px">
      <div class="caption">
        <h3>{{$product->name}}</h3>
        <p>{{$product->description}}</p>
                <h4>₹{{$product->price}}</h4>

        <p><a href="{{route('product.addtocart',['id'=>$product->id])}}" class="btn btn-default" role="button">Add to Cart</a></p>
      </div>
    </div>
  </div>
    @endforeach

</div>
  @endforeach

@endsection