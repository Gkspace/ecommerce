@extends('layouts.master')

@section('content')

@if(Session::has('cart'))

<div class="row">
  <div class="col-sm-6 col-md-4">
   <ul class="list-group">
@foreach ($products as $product)

<li class="panel panel-default"> <div class="panel-body">
<h4>Name:{{$product['item']['name']}}</h4>
<h4>Qty:{{$product['qty']}}</h4>

<h4>Price:{{$product['price']}}</h4></dvi>

</li>
@endforeach

   </ul>

  </div>
  </div>
  <div class="row">
  <div class="col-sm-6 col-md-4">
  <h4>Grand Total:{{$totalprice}}</h4>
<hr />

  </div>
   <div class="row">
  <div class="col-sm-6 col-md-4">
<a class="btn btn-success" href={{route('product.checkout')}}>Checkout</a>
  </div>
  </div>
  @endif
@endsection