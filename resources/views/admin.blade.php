@extends('layouts.master')

@section('content')

<div class="row">
  <div class="col-sm-8">
  <h4>Add a Product</h4>
  <form action="{{route('product.add')}}" method="post">
      
    <div class="form-group">
                  <label class="form-control-label" for="imagepath">Image Path</label>

         <input type="text" class="form-control" id="imagepath" placeholder="imagepath" name="imagepath">
    </div>
     <div class="form-group">
                   <label class="form-control-label" for="name">Name</label>

         <input type="text" id="name" class="form-control" placeholder="name" name="name">
     </div>
      <div class="form-group">
         <label class="form-control-label" for="description">Description</label>

         <input type="text" class="form-control"  placeholder="description" id="description" name="description">
     </div>
      <div class="form-group">
          <label class="form-control-label" for="price">Price</label>
     
         <input type="number" class="form-control"  placeholder="price" id="price" name="price">
     </div>
     {{ csrf_field() }}
        <input type="submit" class="form-control"  value="Add Product">

  </form>

</div>
</div>
<hr />


<div class="container">
<div class="row">
  <h4>View Orders</h4>
@foreach ($orders as $order)
  <div class="col-sm-8">
<div class="card">
  <div class="card-block">

 <h4 class="card-subtitle">CustomerName: {{$order->username}}</h4>
<h4 class="card-subtitle"> Address:{{$order->address}}</h4>
<h4 class="card-subtitle"> Items:</h4>
  <ul>
@foreach ($order->cart->items as $item)

<li class="panel panel-default">
<span class="panel-body">Price:₹{{$item['price']}}
Name:{{$item['item']['name']}}
</span>
</li>
@endforeach

  </ul>
<h4 class="card-subtitle">Total price:{{$order->cart->totalprice}}</h4>
</div>
<hr />

</div>
</div>
@endforeach

</div>
</div>
@endsection