<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',['uses'=>'ProductController@getIndex','as'=>'product.index']);
Route::get('/signup',['uses'=>'UserController@getSignup','as'=>'user.signup','middleware'=>'guest']);
Route::post('/signup',['uses'=>'UserController@postSignup','as'=>'user.signup','middleware'=>'guest']);

Route::get('/signin',['uses'=>'UserController@getSignin','as'=>'user.signin','middleware'=>'guest']);
Route::post('/signin',['uses'=>'UserController@postSignin','as'=>'user.signin','middleware'=>'guest']);


Route::post('/logout',['uses'=>'UserController@getLogout','as'=>'user.logout','middleware'=>'auth']);

Route::get('/admin',['uses'=>'ProductController@getOrders','as'=>'product.orders','middleware'=>'admin']);

Route::post('/admin',['uses'=>'ProductController@addProduct','as'=>'product.add','middleware'=>'admin']);

Route::get('/addtocart/{id}',['uses'=>'ProductController@addtoCart','as'=>'product.addtocart']);

Route::get('/shoppingcart',['uses'=>'ProductController@getshoppingCart','as'=>'product.shoppingcart']);

Route::get('/checkout',['uses'=>'ProductController@getCheckout','as'=>'product.checkout','middleware'=>'auth']);

Route::post('/checkout',['uses'=>'ProductController@postCheckout','as'=>'product.checkout','middleware'=>'auth']);
