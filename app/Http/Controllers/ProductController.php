<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use App\Order;

use App\Cart;

use App\Http\Requests;

use Session;

use Auth;

class ProductController extends Controller
{
    public function getIndex()
    {
        $products=Product::all();
        return view('index',['products'=> $products]);
    }
     public function addProduct(Request $request){

             $product=new Product([
                 'imagepath'=>$request->input('imagepath'),
                                  'name'=>$request->input('name'),
                                  'description'=>$request->input('description'),
                                  'price'=>$request->input('price')

             ]);
             $product->save();

             return redirect()->route('product.index');
    }

public function addtoCart(Request $request,$id){
    $product=Product::find($id);
    $cart=Session::has('cart') ? Session::get('cart'):null;
        if(!$cart)
{
    $cart=new Cart($cart);
}
    $cart->add($product,$product->id);

        Session::put('cart', $cart);

    return redirect()->route('product.index');
}
public function getshoppingCart(){
   if(!Session::has('cart')){
       return view('shoppingcart',['products'=>null]);
   }
   $cart=Session::get('cart');
          return view('shoppingcart',['products'=>$cart->items,'totalprice'=>$cart->totalprice]);

}
public function getCheckout(){
       return view('checkout');
  
}

public function postCheckout(Request $request){
   if(!Session::has('cart'))  {
       return view('shoppingcart');
   }
      $cart=Session::get('cart');
      $order=new Order();
      $order->cart=serialize($cart);
   $order->address=$request->input('address');
      $order->username=$request->input('username');


      Auth::user()->orders()->save($order);

      Session::forget('cart');

      return redirect()->route('product.index')->with('success','Successfully Placed Order!!');

}
  public function getOrders(){
                $orders=Order::all();
           $orders->transform(function($order,$key){
               $order->cart=unserialize($order->cart);
               return $order;
           });

        return view('admin',['orders'=>$orders]);   
    }
}



