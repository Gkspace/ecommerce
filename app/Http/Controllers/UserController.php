<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Auth;
class UserController extends Controller
{
    public function getSignup()
    {
        return view('signup');
    }
    public function postSignup(Request $request){

             $user=new User([
                 'email'=>$request->input('email'),
                                  'password'=>bcrypt($request->input('password')),
             ]);
             $user->save();

             return redirect()->route('product.index');
    }
     public function getSignin()
    {
        return view('signin');
    }
    public function postSignin(Request $request){
        if(Auth::attempt(['email'=>$request->input('email'),'password'=>$request->input('password')])){
                                        return redirect()->route('product.index');

        }
  return redirect()->back();

    }
       public function getLogout()
    {
        Auth::logout();
          return redirect()->back();

    }

  
}
