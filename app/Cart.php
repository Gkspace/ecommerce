<?php

namespace App;


class Cart 
{
    public $items=null;
    public $quantity=0;
    public $totalprice=0;


     public function __constructor($oldcart){
         if($oldcart){

            $this->items=$oldcart->items;
            $this->quantity=$oldcart->quantity;
            $this->totalprice=$oldcart->totalprice;

         }
     }

     public function add($item,$id){
         $itemarray=['qty'=>0,'price'=>$item->price,'item'=>$item];

         if($this->items){
             if(array_key_exists($id,$this->items)){
                   $itemarray=$this->items[$id];
             }
         }
             $itemarray['qty']++;
             $itemarray['price']=$item->price*$itemarray['qty'];
             $this->items[$id]=$itemarray;
             $this->quantity++;
             $this->totalprice+=$item->price;
         
     }
}
